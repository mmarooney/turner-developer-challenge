﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Turner.DeveloperChallenge.Titles.Models;

namespace Turner.DeveloperChallenge.Titles.Controllers
{
    public class TitlesController : ApiController
    {
        private TitlesContext _context = new TitlesContext();

        // GET api/titles
        public IEnumerable<Title> Get(string q = null)
        {
            var titlesList = _context.Titles.ToList();

            if (!string.IsNullOrEmpty(q) && q != "undefined")
                titlesList = 
                    titlesList.Where(t => t.TitleName.ToLower().Contains(q.ToLower())
                                     //|| t.StoryLines.Any(s=>s.Description.ToLower().Contains(q.ToLower()))
                                     //add other fields to search on here           
                    ).ToList();

            return titlesList;
        }

    }
}
